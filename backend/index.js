//Connect to DB and start the server

import app from "./server.js"
import mongodb from "mongodb"
import dotenv from "dotenv"

//Load in Environment Variable 
dotenv.config()
const MongoClient = mongodb.MongoClient

//Set the listening port as per .env file specify or default to 8000
const port = process.env.PORT || 8000


//Setting up the connection to MongoDB
MongoClient.connect(
    process.env.RESTREVIEWS_DB_URI,
    {
        maxPoolSize: 50,
        wtimeoutMS: 2500,
        useNewUrlParser: true
    }
)
.catch(err => {
    console.error(err.stack)
    process.exit(1)
})
.then(async client => {
    app.listen(port, () => {
        console.log('Listening on port ${port}')
    }) // This is how we start our server
}) 