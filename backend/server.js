import express from "express"
import cors from "cors"
import restaurants from "./api/restaurants.route.js"

// This will to make our webserver
const app = express()

app.use(cors())
app.use(express.json()) //Able to read JSON in request body

//Specify route
app.use("/api/v1/restaurants",restaurants)
app.use("*",(req,res) => res.status(404).json({error:"not found"}))

export default app